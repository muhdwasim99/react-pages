import '../../style/common-style.scss';
import { Col, Row, Space } from "antd";
import background from '../../asset/video/crystal-animation-final.gif';
import React, { useState, useEffect } from "react";
import {Redirect } from 'react-router-dom'

export default function Welcome({size}) {

    const [isLoaddone, setLoadDone] = useState(true);

        useEffect(() => {
            const timer = setTimeout(() => {
                setLoadDone(isLoaddone => !isLoaddone);
            }, 27000);
            return () => clearTimeout(timer);
          }, []);

          const close = async() => { setLoadDone(isLoaddone => !isLoaddone); }

            return (
            <div>

            {isLoaddone ? (
            <div class="container">
                <div style={{
                    backgroundImage: `url('${background}')`,
                    height: '99.9vh',
                    
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                    backgroundRepeat: 'no-repeat',
                    padding:0,
                    margin:0 }}>
                </div>
                <button class="btn" onClick={close}>X</button>
            </div>
            ): (
                <Redirect to='/main' />
            )}
            </div>
    );

}

