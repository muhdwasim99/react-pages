import React, { useEffect, useState } from "react";
import Tran1 from "./Tran1";
import Tran2 from "./Tran2";
import Tran3 from "./Tran3";
import Tran4 from "./Tran4";
import Tran5 from "./Tran5";

export default function IndexTran({ size }) {
  const [paddingHor, setPaddingHor] = useState("pd-hor-3rem");
  const [accounts, setAccounts] = useState([]);

  useEffect(() => {
    if (size == "lg") {
      setPaddingHor("pd-hor-3rem");
    } else if (size == "md") {
      setPaddingHor("pd-hor-2rem");
    } else if (size == "sm" || size == "xs") {
      setPaddingHor("pd-hor-1rem");
    }
  }, [size]);
  return (
    <div className={`${paddingHor}`}>
      <Tran1 accounts={accounts} setAccounts={setAccounts} size={size} />
      <Tran2 size={size} />
      <Tran3 size={size} />
      <Tran4 size={size} />
      <Tran5 size={size} />
    </div>
  );
}
