import logo from './logo.svg';
import './App.scss';
import BaseHeader from './component/BaseHeader';
import Section1 from './section/home/Section1';
import Section2 from './section/home/Section2';
import Section3 from './section/home/Section3';
import Section4 from './section/home/Section4';
import Section5 from './section/home/Section5';
import Section6 from './section/home/Section6';
import Section7 from './section/home/Section7';
import Section8 from './section/home/Section8';
import Welcome from './section/home/Welcome';
import BaseFooter from './component/BaseFooter';
import { Switch, Route, useLocation,Redirect } from 'react-router-dom'
import Dao from './section/dao/Dao';
import Eco1 from './section/ecosystem/Eco1';
import IndexEco from './section/ecosystem/IndexEco';
import IndexUsdgPro from './section/usdg-pro/IndexUsdgPro';
import { useEffect, useState } from 'react';
import IndexTran from './section/transparency/IndexTran';
import IndexIncu from './section/incubator/IndexIncu';
import IndexGrant from './section/grants/IndexGrant';


function App() {
  const [size, setSize] = useState('')
  const location = useLocation();
  const [paddingHor, setPaddingHor] = useState('pd-hor-3rem')
  

        
  

  useEffect(() => {
    const body = document.getElementsByTagName('body')[0];

    if (location.pathname.includes('usdg-pro')) {
      body.style.backgroundColor = '#141722'
    }
    else body.style.backgroundColor = '#163554'
  }, [location])

  function getInnerWith() {
    const innerWith = window.innerWidth;
    console.log("innerWidth", innerWith)
    if (innerWith < 576) setSize('xs');
    else if (innerWith >= 576 && innerWith < 768) setSize('sm');
    else if (innerWith >= 768 && innerWith < 992) setSize('md');
    else if (innerWith >= 992 && innerWith < 1200) setSize('lg');
    else setSize('xl')
  }

  useEffect(() => {
    window.addEventListener('resize', getInnerWith)
  }, [])

  useEffect(() => {
    getInnerWith()
  }, [])

  useEffect(() => {
    if (size == 'lg') {
      setPaddingHor('pd-hor-3rem')
    }
    else if (size == 'md') {
      setPaddingHor('pd-hor-2rem')
    }
    else if (size == 'sm' || size == 'xs') {
      setPaddingHor('pd-hor-1rem')
    }
  }, [size])

  return (
    <div className="App">
      <Switch>

      
      <Route path='/' exact>  
          <>
            <Welcome />
          </>
        </Route>

        
          
        <Route path='/main' exact>
          <Redirect to='/main'/>
          <>
            <BaseHeader size={size} />
            
            <div className={`${paddingHor}`}>
              
              <Section1 size={size} />
              <Section2 size={size} />
              <Section3 size={size} />
              <Section4 size={size} />
              <Section5 size={size} />
              <Section6 size={size} />
              <Section7 size={size} />
              <Section8 size={size} />
            </div>

            <BaseFooter size={size} />
          </>
        </Route>
        
        
        <Route path='/dao'>

          <Dao />
        </Route>
        <Route path='/ecosystem'>
          <BaseHeader size={size} />
          <IndexEco size={size} />
          <BaseFooter size={size} />
        </Route>
        
        <Route path='/usdg-pro'>
          <div className='cl-white'>
            <BaseHeader color='white' size={size} />
            <IndexUsdgPro size={size} />
            <BaseFooter size={size} />
          </div>
        </Route>
        <Route path='/transparency'>
          <BaseHeader size={size} />
          <IndexTran size={size} />
          <BaseFooter size={size} />
        </Route>
        <Route path='/incubator'>
          <BaseHeader size={size} />
          <IndexIncu size={size} />
          <BaseFooter size={size} />
        </Route>

        <Route path='/grants'>
          <BaseHeader  size={size} />
          <IndexGrant  size={size} />
          <BaseFooter size={size}  />
        </Route>
      
      </Switch>

    </div>
  );
}

export default App;
